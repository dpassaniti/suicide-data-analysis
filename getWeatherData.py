# AUTOMATICALLY GENERATED FROM JUPYTER NOTEBOOK

#!/usr/bin/env python
# coding: utf-8

# ##### Weather data from https://www.worldweatheronline.com/

# In[2]:


import wwo_hist
import sqlalchemy as db
from datetime import datetime
import pandas as pd

#connect to db
engine = db.create_engine('postgresql://')# put db address here
connection = engine.connect()
metadata = db.MetaData()


# In[12]:


#print database tables

# metadata.reflect(engine)
# for table in metadata.tables.values():
#     print("\n+++ "+table.name+" +++\n")
#     columns = "| "
#     for column in table.c:
#         columns+=column.name+" | "
#     print(columns)


# In[ ]:


# QUERY WEATHER API

from wwo_hist import retrieve_hist_data

#get datetimes
dates = []
for entry in resultSet:
    dates.append(entry[0])

#find earliest and latest cases
begin = dates[0]
end = dates[0]
for date in dates:
    if(date < begin):
        begin = date
    if(date > end):
        end = date

#api request
frequency=24#daily
start_date = begin.strftime("%d-%b-%Y")
end_date = end.strftime("%d-%b-%Y")
api_key = ''#put api key here
location_list = ["35.179,129.075"]#busan longitude,latitude

hist_weather_data = retrieve_hist_data(api_key,
                                location_list,
                                start_date,
                                end_date,
                                frequency,
                                location_label = False,
                                export_csv = True,
                                store_df = True)


# In[ ]:


import numpy as np

#query data from suicidetable
suicidetable = db.Table('suicidetable', metadata, autoload=True, autoload_with=engine)
query = db.select([suicidetable])
resultProxy = connection.execute(query)
resultSet = resultProxy.fetchall()

# list with all case dates
all_case_dates = np.array(resultSet).T[0]
caseDatesInStr = []
for date in all_case_dates:
    caseDatesInStr.append(date.strftime("%Y-%m-%d"))

load weather for all days
weather_df = pd.read_csv("z.csv")
select only relevant days
relevantDays = weather_df[weather_df['date_time'].isin(caseDatesInStr)]

engine.execute('DROP TABLE IF EXISTS weather').fetchall()
insert into database as table called weather
weather_df.to_sql('weather_all', con = engine)
engine.execute("SELECT * FROM weather").fetchall()


# In[11]:


#test query
test = db.Table('weather_all', metadata, autoload=True, autoload_with=engine)
query = db.select([test])
resultProxy = connection.execute(query)
weatherRes = resultProxy.fetchall()
weatherRes

