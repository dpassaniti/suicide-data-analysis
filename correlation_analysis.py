# AUTOMATICALLY GENERATED FROM JUPYTER NOTEBOOK

#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sqlalchemy as db
from matplotlib import pyplot as plt
from matplotlib import dates
import numpy as np
yrsInDbase = 8
#connect to db
engine = db.create_engine('postgresql://')#put db address here
connection = engine.connect()
metadata = db.MetaData()


# In[2]:


# QUERY FROM DB
#query data from suicidetable
suicidetable = db.Table('suicidetable', metadata, autoload=True, autoload_with=engine)
query = db.select([suicidetable])
resultProxy = connection.execute(query)
resultSet = resultProxy.fetchall()

#query kospi https://www.investing.com/indices/kospi-historical-data
KOSPItable = db.Table('kospi_data', metadata, autoload=True, autoload_with=engine)
query = db.select([KOSPItable])
resultProxy = connection.execute(query)
kospires = resultProxy.fetchall()

#query alcohol https://www.who.int/countries/kor/en/
alcoholtable = db.Table('alcohol_consumption_kr', metadata, autoload=True, autoload_with=engine)
query = db.select([alcoholtable])
resultProxy = connection.execute(query)
alcoholres = resultProxy.fetchall()

#query hospitals https://www.ceicdata.com/en/korea/health-statistics-number-of-health-care-centre-by-province-annual/no-of-specialized-hospital-mental-busan
hospitaltable = db.Table('mental_hospitals_busan', metadata, autoload=True, autoload_with=engine)
query = db.select([hospitaltable])
resultProxy = connection.execute(query)
hospitalres = resultProxy.fetchall()

#query monetary https://www.imf.org/external/pubs/ft/weo/2019/02/weodata/weorept.aspx?sy=1980&ey=2024&scsm=1&ssd=1&sort=country&ds=.&br=1&pr1.x=75&pr1.y=18&c=542&s=NGDP_RPCH,PPPGDP,PPPPC,PCPIPCH,LUR,GGXWDG_NGDP&grp=0&a=
monetarytable = db.Table('inter_monetary_fund_kr', metadata, autoload=True, autoload_with=engine)
query = db.select([monetarytable])
resultProxy = connection.execute(query)
monetaryres = resultProxy.fetchall()


# In[3]:


#PREPARE DATA IN LISTS

#monthlyCases
data = []
for r in resultSet:
    data.append(r[0])
data = np.sort(data)
monthlyCases = []
month = -1
for entry in data:
    if entry.month == month:
        monthlyCases[-1] += 1
    else:
        month = entry.month
        monthlyCases.append(1)

#kospivalues
kospivalues = []
for r in kospires:
    kospivalues.append(r['price'])
    
#yearlycases
yrs = []
for entry in resultSet:
    yrs.append(entry[0].year)
yrs = np.sort(yrs)
yearlycases = []
current = -1
for entry in yrs:
    if entry == current:
        yearlycases[-1] += 1
    else:
        current = entry
        yearlycases.append(1)
        
#alcoholvalues !!! 2011~2015
alcoholvalues = []
for r in alcoholres:
    alcoholvalues.append(r['liters_pc'])

#hospitalvalues !!! 2011~2017
hospitalvalues = []
for r in hospitalres:
    hospitalvalues.append(r['num_hospitals'])
    
#monetaryvalues
unemployvalues = []
gdpvalues = []
for r in monetaryres:
    unemployvalues.append(r['unemployment_pcnt'])
    gdpvalues.append(r['gdp_pcnt_change'])


# In[ ]:





# In[4]:


from matplotlib import pyplot
from pandas import Series

from scipy.stats import shapiro
#from scipy.stats import normaltest
from scipy.stats import spearmanr
from scipy.stats import pearsonr
import statsmodels.tsa.stattools as stt

def differencing(data):
    res = []
    for i in range(1,len(data)):
        val = data[i] - data[i-1]
        res.append(val)
    return res
def linkrelatives(data):
    res = []
    for i in range(1,len(data)):
            val = data[i] / data[i-1]
            res.append(val)
    return res
        
def analysis(title, d1, d2):
    print(title)
    alpha = 0.05
    print("\n- TEST NORMALITY (shapiro a = 0.05)\n")
    stat, p = shapiro(d1)
    if(p > alpha):
        inter = "normal"
    else:
        inter = "not normal"
    print('d1: p = {0:.10f}'.format(p), inter)
    stat, p = shapiro(d2)
    if(p > alpha):
        inter = "normal"
    else:
        inter = "not normal"
    print('d2: p = {0:.10f}'.format(p), inter)
    
    print("\n- TEST STATIONARY (dickey fuller a = 0.05)\n")
    res = stt.adfuller(d1)
    if(res[1] > alpha):
        inter = "non stationary"
    else:
        inter = "stationary"
    print('d1: p = {0:.10f}'.format(res[1]), inter)
    res = stt.adfuller(d2)
    if(res[1] > alpha):
        inter = "non stationary"
    else:
        inter = "stationary"
    print('d2: p = {0:.10f}'.format(res[1]), inter)
    
    print("\n- TEST CORRELATION 1\n")
    corr, _ = pearsonr(d1, d2)
    print("pearson corr = ",corr)
    corr, _ = spearmanr(d1, d2)
    print("spearman corr = ",corr)
    
    print("\n- TEST CORRELATION 2 (after detrending)\n")
    
    d1d = differencing(d1)
    d2d = differencing(d2)
    d1l = linkrelatives(d1)
    d2l = linkrelatives(d2)
    
    corr, _ = pearsonr(d1d, d2d)
    print("pearson differencing = ",corr)
    corr, _ = spearmanr(d1d, d2d)
    print("spearman differencing = ",corr)

    corr, _ = pearsonr(d1l, d2l)
    print("pearson link-relatives = ",corr)
    corr, _ = spearmanr(d1l, d2l)
    print("spearman link-relatives = ",corr)


analysis("\n----- month cases | KOSPI -----",monthlyCases, kospivalues)

#NOTE ALCOHOL IS COUNTRY AVERAGE so additional error(+ contrasts with prev studies, low sample size, etc)
#qq plot due to small samples size?
analysis("\n----- yearly cases(reduced) | alcohol -----",yearlycases[0:5], alcoholvalues)

analysis("\n----- yearly cases (reduced) | ment hospitals -----",yearlycases[0:7], hospitalvalues)

analysis("\n----- year cases | unemployment -----", yearlycases, unemployvalues)

analysis("\n----- year cases | gdp % change -----", yearlycases, gdpvalues)


# In[5]:


# get weather data
def analysis2(title, d1, d2):
    print(title)
    alpha = 0.05
    print("\n- TEST NORMALITY (shapiro a = 0.05)\n")
    stat, p = shapiro(d1)
    if(p > alpha):
        inter = "normal"
    else:
        inter = "not normal"
    print('d1: p = {0:.10f}'.format(p), inter)
    stat, p = shapiro(d2)
    if(p > alpha):
        inter = "normal"
    else:
        inter = "not normal"
    print('d2: p = {0:.10f}'.format(p), inter)
    
    print("\n- TEST STATIONARY (dickey fuller a = 0.05)\n")
    res = stt.adfuller(d1)
    if(res[1] > alpha):
        inter = "non stationary"
    else:
        inter = "stationary"
    print('d1: p = {0:.10f}'.format(res[1]), inter)
    res = stt.adfuller(d2)
    if(res[1] > alpha):
        inter = "non stationary"
    else:
        inter = "stationary"
    print('d2: p = {0:.10f}'.format(res[1]), inter)
    
    print("\n- TEST CORRELATION 1\n")
    corr, _ = pearsonr(d1, d2)
    print("pearson corr = ",corr)
    corr, _ = spearmanr(d1, d2)
    print("spearman corr = ",corr)
    
temps =	{}
tempTotals = {}

# get weather data per each case
weather = db.Table('weather_all', metadata, autoload=True, autoload_with=engine)
for case in resultSet:
    day = case[0].strftime("%Y-%m-%d")
    query = db.select([weather.columns.maxtempC, weather.columns.mintempC]).where(weather.columns.date_time == day)
    tempTuple = connection.execute(query).fetchall()
    avgT = (tempTuple[0][0]+tempTuple[0][1])/2.0
    if(avgT in temps):
        temps[avgT] += 1
    else:
        temps[avgT] = 1

# count days per avg temp
query = db.select([weather.columns.maxtempC, weather.columns.mintempC])
tempTuples = connection.execute(query).fetchall()
for t in tempTuples:
    avgT = (t[0]+t[1])/2.0
    if(avgT in temps.keys()):
        if(avgT in tempTotals):
            tempTotals[avgT] += 1
        else:
            tempTotals[avgT] = 1
            
shrs = {}
shYearTot = {}

# get weather data per each case
for case in resultSet:
    day = case[0].strftime("%Y-%m-%d")
    query = db.select([weather.columns.sunHour]).where(weather.columns.date_time == day)
    sunHours = connection.execute(query).fetchall()
    if(sunHours[0][0] in shrs):
        shrs[sunHours[0][0]] += 1
    else:
        shrs[sunHours[0][0]] = 1

# count days per avg temp
query = db.select([weather.columns.sunHour])
sunHours = connection.execute(query).fetchall()
for sh in sunHours:
    if(sh[0] in shrs.keys()):
        if(sh[0] in shYearTot):
            shYearTot[sh[0]] += 1
        else:
            shYearTot[sh[0]] = 1


# In[6]:


# TEMPERATURE

xs = []
ys = []
for temp, count in temps.items():
    xs.append(temp)
    ys.append(count/yrsInDbase)    
    
indxs = np.argsort(xs)
xs = np.sort(xs)
ys = [ys[i] for i in indxs]

analysis2("\n----- yearly cases by temp -----", xs, ys)

xs = []
ys = []
for temp, count in temps.items():
    count/=tempTotals[temp]
    xs.append(temp)
    ys.append(count)    
    
indxs = np.argsort(xs)
xs = np.sort(xs)
ys = [ys[i] for i in indxs]

analysis2("\n-----yearly cases by temp (NORMALIZED) -----", xs, ys)


# In[7]:


xs = []
ys = []
for indx, count in shrs.items():
    xs.append(indx)
    ys.append(count/yrsInDbase)    
    
indxs = np.argsort(xs)
xs = np.sort(xs)
ys = [ys[i] for i in indxs]

analysis2("\n----- yearly cases by dayly sun hrs -----", xs, ys)

xs = []
ys = []
for indx, count in shrs.items():
    count/=shYearTot[indx]
    xs.append(indx)
    ys.append(count)
    
indxs = np.argsort(xs)
xs = np.sort(xs)
ys = [ys[i] for i in indxs]

analysis2("\n----- yearly cases by dayly sun hrs (NORMALIZED) -----", xs, ys)

